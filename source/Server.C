// Server.C  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Server.H"

Server::Server(String h, int p, String pass)
  : hostName(h), port(p), password(pass)
{ }

String
Server::getHostName() const
{
  return hostName;
}

int
Server::getPort() const
{
  return port;
}

String
Server::getPassword() const
{
  return password;
}
