// UserList.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (C) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef USERLIST_H
#define USERLIST_H

#include <ctime>
#include <list>

#include "String.H"
#include "UserListItem.H"

class Commands;
class UserCommands;

class UserList {
  String listFilename;
  std::list<UserListItem *> l;
  
public:
  UserList(String);
  ~UserList();

  void read();
  void save();
  void clear();

  void addUser(String, String, int, int, bool,
               std::time_t = -1, String = "");
  void addUserFirst(String, String, int, int, bool,
                    std::time_t = -1, String = "");
  void removeFirst();
  void removeUser(String, String);

  UserListItem * getUserListItem(String, String);

  int getMaxLevel(String);
  int getLevel(String, String);

  int getMaxProt(String, String);

  bool isInUserList(String, String = "");

  friend class Commands;
  friend class UserCommands;
};

#endif
