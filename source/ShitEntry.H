// ShitEntry.H  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (c) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef SHITENTRY_H
#define SHITENTRY_H

#include <ctime>

#include "Mask.H"

class ShitList;
class UserCommands;

class ShitEntry {
  Mask shitMask;
  Mask shitChannelMask;
  int shitLevel;
  std::time_t expirationDate;
  String shitReason;

public:

  enum {
    SHIT_NOSHIT  = 0,
    SHIT_NOOP    = 1,
    SHIT_NOJOIN  = 2,
    SHIT_NODEBAN = 3
  };

  ShitEntry(String, String, int, std::time_t, String);
  
  bool matches(String, String);
  bool isStillValid();

  String getMask();
  String getChannelMask();

  int getShitLevel() const;
  time_t getExpirationDate() const;
  String getShitReason() const;

  friend class ShitList;
  friend class UserCommands;
};

#endif
