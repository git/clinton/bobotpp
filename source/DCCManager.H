// DCCManager.H  -*- C++ -*-
// Copyright (c) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef DCC_MANAGER_H
#define DCC_MANAGER_H

#include <list>
#include <map>

#include <sys/types.h>
#include <unistd.h>

#include "String.H"
#include "UserCommands.H"

class DCCConnection;
class DCCPerson;
class Person;
class ServerConnection;

typedef std::map<String, DCCPerson*, std::less<String> > DCC_MAP;

// Manages all active DCC connections and allows you to send a message
// to each one using the string representation of the user's address
class DCCManager
{
  DCC_MAP dcc_map;
public:
  void addConnection (DCCConnection*);
  bool sendMessage (String, String);
  void checkStale ();
#ifdef _HPUX_SOURCE
  void checkInput (int);
#else
  void checkInput (fd_set);
#endif
  ~DCCManager ();

  friend class Bot;
  friend void
  UserCommands::DCCList (ServerConnection *, Person *,
			 String, String);
};

#endif
