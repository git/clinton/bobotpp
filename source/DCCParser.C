// DCCParser.C  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (C) 2002,2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "DCCParser.H"

#include "DCCConnection.H"
#include "DCCPerson.H"
#include "Parser.H"
#include "Utils.H"

#ifdef USESCRIPTS
#include "BotInterp.H"
#endif

void
DCCParser::parseLine(DCCConnection *cnx, String line)
{
  Person * from = new DCCPerson(cnx);
#ifdef USESCRIPTS
  // Call hooks/dcc/chat-message hook functions
  cnx->get_bot()->botInterp->RunHooks (Hook::DCC_CHAT_MESSAGE, 
				 from->getAddress () + " " + line,
				 scm_list_n 
				       (Utils::str2scm (from->getAddress ()),
					Utils::str2scm (line),
					SCM_UNDEFINED));
#endif
  Parser::parseMessage(cnx->get_bot()->serverConnection, 
		       from, cnx->get_bot()->nickName,
                       line);
  delete from;
}
