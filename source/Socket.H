// Socket.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SOCKET_H
#define SOCKET_H

#include "String.H"

class Socket {
  struct s_fd {
    int fd; // the data
    int n; // reference counter
    s_fd() { n = 1; }
  };
  s_fd *fd;

  int remotePort;
  unsigned long remoteAddress;

  int localPort;
  unsigned long localAddress;

  int begin, end;


public:
  Socket();
  Socket(int, unsigned long, int);
  Socket(const Socket &);

  ~Socket();

  int getRemotePort() const;
  int getLocalPort() const;
  int getFileDescriptor() const;

  bool isConnected();

  bool setRemoteHostname(String);
  bool setRemoteIP(unsigned long);
  bool setRemotePort(int);

  bool setLocalHostname(String);
  bool setLocalIP(unsigned long);
  bool setLocalPort(int);

  bool setNonBlocking(bool = true);

  bool connect();
  bool listen(int = 5);
  Socket accept();
  void close();
  
  bool write(String, bool = false);
  String readLine();
  String readChar();
  bool hasData();
private:
  Socket(Socket &);
};

#endif
