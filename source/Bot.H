// Bot.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (c) 2002,2003 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef BOT_H
#define BOT_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <ctime>
#include <set>
#include <fstream>
#include <string>
#include <map>

#include "BotThreading.H"
#include "String.H"

#define VERSION_STRING PACKAGE " version " VERSION " by unknown_lamer@FreeNode <clinton@unknownlamer.org>\n1.97 and below by eb@IRCNet <eb@via.ecp.fr>"
#define COPYRIGHT_STRING PACKAGE " version " VERSION ", Copyright (C) 1997-2000 Etienne BERNARD\nCopyright (C) 2002,2003,2004,2005,2008,2009,2020 Clinton Ebadi"

class Channel;
class ChannelList;
class Commands;
class DCCConnection;
class DCCManager;
class DCCParser;
class Parser;
class Person;
class ServerConnection;
class ServerList;
class ShitList;
class UserList;
class UserCommands;

class userFunction;
class wantedChannel;

#ifdef USESCRIPTS
class BotInterp;
#endif

class Bot {
public:
  String nickName;
  String wantedNickName;
  String userName;
  String ircName;
  String userHost;
  String localIP;
  char commandChar;

  String userListFileName;
  String shitListFileName;
  String helpFileName;
  String initFileName;

  std::ofstream logFile;

  bool connected;
  bool debug;
  bool stop;
  bool sentPing;

  ChannelList * channelList;
  UserList * userList;
  ShitList * shitList;
  ServerList * serverList;
#ifdef USESCRIPTS
  BotInterp * botInterp;
#endif
  std::map<std::string, class userFunction*, 
	   std::less<std::string> > userFunctions;
  std::map<String, wantedChannel *, std::less<String> > wantedChannels;

  std::map<String, unsigned int, std::less<String> > ignoredUserhosts;
  std::map<String, Person *, std::less<String> > spyList;

  std::time_t startTime, currentTime, lastNickNameChange, lastChannelJoin;

  ServerConnection * serverConnection;
  DCCManager* dccConnections;

  std::map<unsigned long, String, std::less<unsigned long> > userhostMap;
  unsigned long sentUserhostID;
  unsigned long receivedUserhostID;

  static unsigned int MAX_MESSAGES;
  static unsigned int MAX_NICKLENGTH;
  static const std::time_t NICK_CHANGE  = 60;
  static const std::time_t CHANNEL_JOIN = 60;
  static const std::time_t IGNORE_DELAY = 180;
  static const std::time_t DCC_DELAY = 300;
  static const std::time_t PING_TIME = 90;
  static const std::time_t TIMEOUT = 120;

  static const std::string release_revision;

private:
  String logFileName;
  String logs_dir;
  String configFileName;
  String versionString;

  // mutexes (one per settable variable, although a global lock on bot
  // state might not be unreasonable since setting is so rare)
  mutable BotMutex version_mutex;

#ifdef USESCRIPTS
  String scriptLogFileName;
  String autoexecFileName;
#endif

public:

  Bot(String, bool=false);
  ~Bot();

  void run();

  void waitForInput();

  void logLine(String);
  void readConfig();

  void reconnect();

  // getters and setters
  void set_log_dir (String);
  void set_log_file (String);

  String get_version () const;
  void set_version (String);

  friend class Parser;
  friend class DCCParser;
  friend class Person;
  friend class Channel;
  friend class ServerConnection;
  friend class UserCommands;
  friend class Commands;
  
private:
  bool canChangeServer();
  void nextServer();
  void connect(int);

  enum DCC_TYPE { CHAT };
  void addDCC(Person *, unsigned long, int, int);

  void rehash();
  String getUserhost(String, String);
  bool iAmOp(String);

  void init_user_functions ();
  void destroy_user_functions ();
};

#endif

