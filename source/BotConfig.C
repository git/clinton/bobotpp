// BotConfig.C  -*- C++ -*-
// Copyright (C) 2004,2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "BotConfig.H"
#include "Bot.H"
#include "StringTokenizer.H"
#include "Utils.H"
#include <iostream>
#include <string>
#include <list>
#include <algorithm>

BotConfig::BotConfig (std::string cf)
  : config_filename (cf)
{ }

bool
BotConfig::read_config ()
{
  std::ifstream config_file (config_filename.c_str ());
  std::string current_line;
  unsigned int line_number = 1;

  options_db.clear ();

  if (!config_file)
    {
      // FIXME: log lines (needs local bot object, probably a
      // redesign. UGH).
      
      // Bot::logLine ("Error: Config File Not Found `" +
      // config_filename + "'");
      return false;
    }

  while (!config_file.eof ())
    {
      std::getline (config_file, current_line);
      if (current_line.length () == 0 || current_line[0] == '#')
	{
	  line_number++;
	  continue;
	}
      
      StringTokenizer st (current_line);
      std::string command = Utils::to_upper (Utils::trim_str(st.next_token('=')));
      StringTokenizer params (Utils::trim_str (st.next_token('=')));

      set_option_value (command, params.rest (), true);
    }
  return true;
}


BotConfig::t_option_values
BotConfig::get_option_values (std::string key)
{
  t_options_db::const_iterator cf_iter = 
    options_db.find (key);

  if (cf_iter != options_db.end ())
    return cf_iter->second;
  else
    return t_option_values (); // Empty vector
}

std::string
BotConfig::set_config_file (std::string fname)
{
  std::string old_config_filename = config_filename;
  config_filename = fname;
  options_db.clear ();
  read_config ();
  return old_config_filename;
}

namespace
{
  struct push_back_
  {
    BotConfig::t_value_list* lst;
    
    push_back_ (BotConfig::t_value_list* l)
      : lst (l) 
    {}

    void operator() (BotConfig::t_value val)
    {
      lst->push_back (val);
    }
  };
  
  struct run_fun_
  {
    std::string key;
    BotConfig::t_value_list& vals;
    bool appended;
    
    run_fun_ (std::string k, BotConfig::t_value_list& vl, bool a)
      : key (k), vals (vl), appended (a) 
    { }
    void operator() (BotConfig::t_watcher w)
    {
      w (key, vals, appended);
    }
  };
}

void
BotConfig::set_option_value (std::string key, t_value_list values, 
				  bool append)
{
  key = Utils::to_upper (key); // keys are case insensitive
  t_options_db::iterator cf_iter = options_db.find (key);
  

  if (cf_iter != options_db.end ())
    {
      if (append)
	{
	  // FIXME: Potentially slow (make this push_front in 3.0)
	  std::for_each (values.begin (), values.end (),
			 push_back_ (&cf_iter->second.first));
	}
      else
	cf_iter->second.first = values;

      // call watchers
      std::for_each (cf_iter->second.second.begin (),
		     cf_iter->second.second.end (),
		     run_fun_ (key, values, append));
    }
  else
    options_db[key] = t_option_values (values, t_watcher_list ());
}

void
BotConfig::set_option_value (std::string key, t_value value, bool append)
{
  t_value_list value_list = t_value_list ();
  value_list.push_front (value);

  set_option_value (key, value_list, append);
}

bool
BotConfig::add_watcher (std::string key, t_watcher new_watcher)
{
  t_options_db::iterator cf_iter = options_db.find (key); 

  if (cf_iter != options_db.end ())
    {
      cf_iter->second.second.push_front (new_watcher);
      return true;
    }
  else
    return false;
}
