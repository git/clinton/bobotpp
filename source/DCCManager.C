// DCCManager.H  -*- C++ -*-
// Copyright (c) 2002,2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#include "DCCManager.H"

#include "DCCConnection.H"
#include "DCCPerson.H"
#include "UserCommands.H"
#include "Utils.H"

#ifdef USESCRIPTS
#include "BotInterp.H"
#endif

void
DCCManager::addConnection (DCCConnection *cnx)
{
  DCCPerson *person = new DCCPerson (cnx);
  String temp = person->getAddress ();

  if (dcc_map[temp])
    {
      delete dcc_map[temp];
    }

  dcc_map[person->getAddress ()] = person;
}

bool
DCCManager::sendMessage (String to, String message)
{
  DCCPerson *person = dcc_map[to];
  if (!person)
    return false;

  person->sendNotice (message);
  return true;
  return false;
}

// checks for stale connections and removes them
void
DCCManager::checkStale ()
{
  DCC_MAP::iterator it, it2;
  DCCConnection const* temp_cnx;

  for (it = dcc_map.begin (),it2 = dcc_map.end ();
       it != it2; ++it)
    {
      temp_cnx = it->second->dcc;
      if (temp_cnx->get_autoRemove () && std::time (0) >= 
	  (std::time_t)(temp_cnx->get_lastSpoken () + Bot::DCC_DELAY))
	{
#ifdef USESCRIPTS
	  DCCPerson *dp = new DCCPerson ((DCCConnection *) temp_cnx);

	  // run hooks/dcc/chat-end
	  dp->bot->botInterp->RunHooks 
	    (Hook::DCC_CHAT_END, 
	     dp->getAddress (),
	     scm_list_n (Utils::
			 str2scm (dp->getAddress ()),
			 SCM_UNDEFINED));
	  delete dp;
#endif
	  delete it->second;
	  dcc_map.erase (it);
	}
    }
}

void
#ifdef _HPUX_SOURCE
DCCManager::checkInput (int rd)
#else
DCCManager::checkInput (fd_set rd)
#endif
{

  DCC_MAP::iterator it = dcc_map.begin();
  DCC_MAP::iterator it2;
  
  while (it != dcc_map.end ())
    {
      it2 = it;
      ++it;
#ifdef _HPUX_SOURCE
      if (rd & it2->second->dcc->getFileDescriptor())
#else
	if (FD_ISSET(it2->second->dcc->getFileDescriptor(), &rd))
#endif
	  {
	    if (it2->second->handleInput()) 
	      {
#ifdef USESCRIPTS
		DCCPerson *dp = new DCCPerson (*it2->second);

		// run hooks/dcc/chat-end
		dp->bot->botInterp->RunHooks 
		  (Hook::DCC_CHAT_END, 
		   dp->getAddress (),
		   scm_list_n (Utils::
			       str2scm (dp->getAddress ()),
			       SCM_UNDEFINED));
		delete dp;
#endif

		delete it2->second->dcc;
		delete it2->second;
		dcc_map.erase(it2);
	      }
	  }
    }
}

DCCManager::~DCCManager ()
{
  DCC_MAP::iterator it, it2;
  it = dcc_map.begin ();
  it2 = dcc_map.end();

  for (; it != it2; ++it)
    {
      delete it->second->dcc;
      delete it->second;
    }
}
