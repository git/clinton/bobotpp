// DCCConnection.H  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (c) 2002,2003 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef DCCCONNECTION_H
#define DCCCONNECTION_H

#include <ctime>

#include "Bot.H"
#include "Connection.H"

class DCCConnection : public Connection {
protected:
  Bot *bot;
  String nuh;
  std::time_t lastSpoken;
  bool autoRemove;
  DCCConnection (unsigned long, int);

public:
  DCCConnection(Bot *, String, unsigned long, int);

  virtual bool connect()=0;
  virtual bool handleInput()=0;

  virtual void sendNotice(String)=0;

  // Accessors
  virtual Bot* get_bot () const { return bot; };
  virtual std::time_t get_lastSpoken () const { return lastSpoken; };
  virtual bool get_autoRemove () const { return autoRemove; };
  virtual String get_nuh () const { return nuh;};
  // Setters
  virtual void set_autoRemove (bool new_ar) { autoRemove = new_ar;};

  //  friend class Bot;
  //  friend class DCCPerson;
  //  friend class DCCParser;
  //  friend class UserCommands;
  //  friend class DCCManager;
};

#endif
