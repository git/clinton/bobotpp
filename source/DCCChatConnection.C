// DCCConnection.C  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (C) 2002,2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "DCCChatConnection.H"

#include <ctime>
#include <string>

#include "DCCParser.H"
#include "DCCPerson.H"
#include "Person.H"
#include "Socket.H"
#include "User.H"
#include "Utils.H"

#ifdef USESCRIPTS
#include "BotInterp.H"
#endif


DCCChatConnection::DCCChatConnection(Bot *b, String n, unsigned long address, 
				     int port)
  : DCCConnection(b, n, address, port)
{ }

bool
DCCChatConnection::connect()
{
  if (!socket.connect())
    return false;

  DCCPerson *from = new DCCPerson (this);

  if (Utils::get_level (bot, from->getAddress()) < User::FRIEND)
    {
      from->sendNotice 
	("\002You do not have permission to DCC CHAT the bot\002");
      delete from;
      return false;
    }

#ifdef USESCRIPTS
  // run hooks/dcc/chat-begin
  bot->botInterp->RunHooks (Hook::DCC_CHAT_BEGIN, 
			    from->getAddress (),
			    scm_list_n (Utils::
					str2scm (from->getAddress ()),
					SCM_UNDEFINED));
#endif


  delete from;
  return true;
}

bool
DCCChatConnection::handleInput()
{
  std::string line = socket.readLine();

  lastSpoken = time(0);

  if (line.length() == 0)
    return true;

  if (bot->debug)
    std::cout << "DCC: <" << line << ">" << std::endl;

  DCCParser::parseLine(this, line);

  return false;
}

void
DCCChatConnection::sendNotice(String message)
{
  socket.write(message, true);
}
