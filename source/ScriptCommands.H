// ScriptCommands.H  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (c) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef SCRIPTCOMMANDS_H
#define SCRIPTCOMMANDS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef USESCRIPTS

#include <libguile.h>

#include <functional>

class User;

class ScriptCommands {
public:
  static SCM Action(SCM, SCM);
  static SCM AddUser(SCM, SCM, SCM, SCM, SCM, SCM, SCM);
  static SCM AddServer(SCM, SCM);
  static SCM AddShit(SCM, SCM, SCM, SCM, SCM);
  static SCM Ban(SCM, SCM);
  static SCM ChangeCommandLevel(SCM, SCM);
  static SCM ChannelUsers (SCM);
  static SCM CTCP(SCM, SCM, SCM);
  static SCM CTCPReply(SCM, SCM, SCM);
  static SCM Cycle(SCM);
  static SCM Deban(SCM, SCM);
  static SCM DelServer(SCM);
  static SCM DelUser(SCM, SCM);
  static SCM DelShit(SCM, SCM);
  static SCM Deop(SCM, SCM);
  static SCM Die(SCM);
  static SCM Do(SCM);
  static SCM Invite(SCM, SCM);
  static SCM Join(SCM, SCM);
  static SCM Keep(SCM, SCM);
  static SCM Kick(SCM, SCM, SCM);
  static SCM KickBan(SCM, SCM, SCM);
  static SCM Lock(SCM);
  static SCM LogPort(void);
  static SCM Mode(SCM, SCM);
  static SCM Msg(SCM, SCM);
  static SCM NextServer(void);
  static SCM Nick(SCM);
  static SCM Notice (SCM, SCM);
  static SCM Op(SCM, SCM);
  static SCM Part(SCM);
  static SCM Reconnect(void);
  static SCM Say(SCM, SCM);
  static SCM Server(SCM); // ACK NAMING CLASH
  static SCM SetFloodRate(SCM);
  static SCM SetVersion(SCM);
  static SCM TBan(SCM, SCM, SCM);
  static SCM TKBan(SCM, SCM, SCM, SCM);
  static SCM Topic(SCM, SCM);
  static SCM Unlock(SCM);
  static SCM Who(SCM);
  static SCM Whois(SCM);

  static SCM getNickname(void);
  static SCM getReleaseRevision(void);
  static SCM getServer(void);
  static SCM getServerList(void);
  static SCM getVersion(void);
  static SCM flushQueue(void);
  static SCM flushPort(void);
  static SCM random(SCM);
  static SCM addCommand(SCM, SCM, SCM, SCM, SCM);
  static SCM delCommand(SCM);
  static SCM AddHook(SCM, SCM, SCM, SCM, SCM, SCM);
  static SCM AddTimer(SCM, SCM);
  static SCM DelTimer(SCM);

  // DCC CHAT
  static SCM sendDCCChatMessage (SCM, SCM);

  /*
  SCM sendChannelMode(SCM);
  SCM sendChannelMode(SCM, SCM, SCM);
  SCM sendInvite(SCM, SCM);
  SCM sendJoin(SCM, SCM);
  SCM sendKick(SCM, SCM, SCM);
  SCM sendNick(SCM);
  */
  /*
  SCM sendPart(SCM);
  SCM sendPass(SCM);
  SCM sendPing(SCM);
  SCM sendPong(SCM);
  SCM sendPrivmsg(SCM, SCM);
  SCM sendQuit(SCM);
  SCM sendTopic(SCM, SCM);
  SCM sendUser(SCM, SCM);
  SCM sendUserMode(SCM, SCM);
  SCM sendUserhost(SCM);
  SCM sendWho(SCM);
  SCM sendWhois(SCM);
  */
private:
  struct collect_channel_users : std::binary_function<SCM*, User, void>
  {
    void operator() (SCM*, User) const;
  };
};

#endif
#endif
