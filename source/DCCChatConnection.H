// DCCChatConnection.H  -*- C++ -*-
// Copyright (c) 2003 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef DCCCHATCONNECTION_H
#define DCCCHATCONNECTION_H

#include <ctime>

#include "DCCConnection.H"
#include "String.H"

class Bot;
class UserCommands;

class DCCChatConnection : public DCCConnection {
public:
  DCCChatConnection(Bot *, String, unsigned long, int);

  bool connect();
  bool handleInput();

  void sendNotice(String);

//   friend class Bot;
//   friend class DCCPerson;
//   friend class DCCParser;
//   friend class UserCommands;
//   friend class DCCManager;
};

#endif
