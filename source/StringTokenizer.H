// StringTokenizer.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (c) 2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef STRINGTOKENIZER_H
#define STRINGTOKENIZER_H

#include <string>

class StringTokenizer
{
  std::string st;
  std::string::size_type pos;
public:
  StringTokenizer(std::string);
  StringTokenizer(StringTokenizer &);
  
  bool more_tokens_p ();
  bool more_tokens_p (char);
  
  unsigned int count_tokens();
  unsigned int count_tokens(char);
  
  std::string next_token();
  std::string next_token(char, bool = false);
  
  std::string rest();
};

#endif
