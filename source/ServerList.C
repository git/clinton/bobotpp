// ServerList.C  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (C) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "ServerList.H"

#include "Server.H"

ServerList::ServerList()
  : it(v.end()), current(0), currentNumber(0)
{ }

ServerList::~ServerList()
{
  Server *s;
  for (it = v.begin(); it != v.end(); ++it) {
    s = *it;
    delete s;
  }
}

void
ServerList::addServer(Server *s)
{
  v.push_back(s);
  it = v.end(); // move to end because iterators are invalidated
}

void
ServerList::delServer(int n)
{
  v.erase(v.begin () + n);
}

Server *
ServerList::nextServer()
{
  if (v.empty())
    return 0;
  
  if (it == v.end()) {
    it = v.begin();
    currentNumber = 0;
  } else
    currentNumber++;

  return current = *(it++);
}

Server *
ServerList::currentServer()
{
  return current;
}
