// Commands.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (C) 2002,2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef COMMANDS_H
#define COMMANDS_H

#include <ctime>

#include "String.H"

class Bot;
class Channel;
class Mask;
class Message;
class User;

class Commands {
public:
  static Message Action(Bot *, String, String);
  static Message AddUser(Bot *, String, String, int, int, bool, 
			 std::time_t, String);
  static Message AddServer(Bot *, String, int);
  static Message AddShit(Bot *, String, String, int, std::time_t, String);
  static Message AddServer(Bot *, String, String);
  static Message Ban(Bot *, String, String);
  static Message ChangeLevel(Bot *, String, String, int);
  static Message CTCP (Bot*, std::string, std::string, std::string);
  static Message CTCPReply (Bot*, std::string, std::string, std::string);
  static Message Cycle(Bot *, String);
  static Message Deban(Bot *, String, String);
  static Message DelServer(Bot *, int);
  static Message DelUser(Bot *, String, String);
  static Message DelShit(Bot *, String, String);
  static Message Deop(Bot *, String, String);
  static Message Die(Bot *, String = "Leaving");
  static Message Do(Bot *, String);
  static Message Invite(Bot *, String, String);
  static Message Join(Bot *, String, String = "");
  static Message Keep(Bot *, String, String);
  static Message Kick(Bot *, String, String, String);
  static Message KickBan(Bot *, String, String, String);
  static Message Lock(Bot *, String);
  static Message Mode(Bot *, String, String);
  static Message Msg(Bot *, String, String);
  static Message NextServer(Bot *);
  static Message Nick(Bot *, String);
  static Message Notice(Bot *, String, String);
  static Message Op(Bot *, String, String);
  static Message Part(Bot *, String);
  static Message Reconnect(Bot *);
  static Message Say(Bot *, String, String);
  static Message Server(Bot *, int);
  static Message SetVersion(Bot *, String);
  static Message SetFloodRate(Bot*, unsigned int);
  static Message TBan(Bot *, String, String, int);
  static Message TKBan(Bot *, String, String, int, String);
  static Message Topic(Bot *, String, String);
  static Message Unlock(Bot *, String);
  static Message Who (Bot *, String);
  static Message Whois (Bot *, String);

private:
  class deop_wildcard : std::unary_function<User &, void>
  {
    Bot *bot;
    Mask &mask;
    const String channel;
  public:
    deop_wildcard (Bot*, Mask&, String&);
    void operator ()(const User &);
  };

  class kick_wildcard : std::unary_function<User &, void>
  {
    Bot *bot;
    Mask &mask;
    const String channel;
    const String reason;
  public:
    kick_wildcard (Bot*, Mask&, String&, String&);
    void operator ()(const User &);
  };
};

#endif
