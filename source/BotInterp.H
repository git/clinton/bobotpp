// BotInterp.H  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (C) 2002,2005,2008 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef BOTINTERP_H
#define BOTINTERP_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef USESCRIPTS

#include <functional>
#include <list>
#include <map>
#include <string>

#include <ctime>

#include <libguile.h>

#include "BotThreading.H"
#include "String.H"
#include "Utils.H"

class Bot;

struct Hook {
  int type;
  int priority;
  bool fallthru;

  std::string regex_str;
  std::string name;
  SCM regex;
  SCM function;

  Hook(int t, std::string rs, SCM r, SCM f, int p, bool ft, std::string n="DEFAULT")
    : type(t), priority (p), fallthru (ft), regex_str(rs), 
      name (n), regex(r), function(f)  { }

  bool operator< (const Hook & h) const;

  enum {
    ACTION, NICKNAME, SIGNOFF, CTCP, CTCP_REPLY,
    DISCONNECT, FLOOD, INVITE, JOIN, KICK, MODE, 
    MESSAGE, NAMES, NOTICE, PART, PUBLIC,
    PUBLIC_NOTICE, RAW, TIMER, TOPIC,
    // send hooks
    SEND_ACTION, SEND_CTCP, SEND_PUBLIC, SEND_MESSAGE,
    SEND_WHO, SEND_WHOIS,
    // DCC hooks
    DCC_CHAT_BEGIN, DCC_CHAT_END, DCC_CHAT_MESSAGE
  };  
};

struct Timer {
  int count;
  std::time_t when;
  SCM function;

  Timer(int c, std::time_t t, SCM f)
    : count(c), when(t), function(f) { }

  bool operator< (const Timer & other) const
  { return when < other.when; }
};

class BotInterp {
  typedef std::list<Timer *> TimerList;
  typedef std::list<Hook *> HookList;
  typedef std::map<int, HookList, std::less<int> > HookMap;
  
  Bot * bot;
  SCM logPort;
  int counter;

  HookMap hooks;
  TimerList timers;

  Utils::IndirectPred<Timer, std::less<Timer> > timer_sort_p;
  Utils::IndirectPred<Hook, std::less<Hook> > hook_sort_p;

  BotMutex hook_mutex;
  BotMutex timer_mutex; // NOTE: recursive lock

public:
  BotInterp(Bot *, String);

  SCM ScriptLog();

  void Execute(String);
  void LoadScript(String);

  bool AddHook(int, SCM, SCM, int, bool, std::string);
  bool RunHooks(int, std::string, SCM);

  SCM AddTimer(int, SCM);
  bool DelTimer(SCM);
  bool RunTimers(int);

  friend class Interp;
  friend class ScriptCommands;
};

#endif
#endif
