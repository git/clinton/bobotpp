#
# Copyright (c) 1998 Etienne Bernard
# Copyright (c) 2002 Clinton Ebadi
#

# Check if we have a decent C++ compiler
# that supports Standard Template Library (STL)
AC_DEFUN([AC_CPP_STL],
[
  AC_MSG_CHECKING([for STL support])

  AC_CACHE_VAL(ac_cv_cpp_stl,
  [
    AC_LANG_CPLUSPLUS
    cat > conftest.$ac_ext <<EOF
    #include <list>
    int main() {
      std::list<int> l;
      return 0;
    };
EOF


    if AC_TRY_EVAL(ac_link) && test -s conftest; then
      ac_cv_cpp_stl="have_stl=yes"
    else
      ac_cv_cpp_stl="have_stl=no"
    fi
    rm -f conftest*
  ])dnl
  eval "$ac_cv_cpp_stl"

  AC_MSG_RESULT([$have_stl])

  if test "$have_stl" != "yes"; then
    AC_MSG_ERROR([Sorry, you need STL support with your compiler])
  fi
])


# Check is compiler has ios_base
AC_DEFUN([AC_CPP_STL_IOSBASE],
[
  AC_MSG_CHECKING([for std::ios_base in iostreams])

  AC_CACHE_VAL(ac_cv_cpp_iosbase,
  [
    AC_LANG_CPLUSPLUS
    cat > conftest.$ac_ext <<EOF
    #include <iostream>
    #include <fstream>
    int main() {
      std::ifstream file ("README", std::ios_base::in);
      return 0;
    };
EOF

    if AC_TRY_EVAL(ac_link) && test -s conftest; then
      ac_cv_cpp_iosbase="have_iosbase=yes"
    else
      ac_cv_cpp_iosbase="have_iosbase=no"
    fi
    rm -f conftest*
  ])dnl
  eval "$ac_cv_cpp_iosbase"

  AC_MSG_RESULT([$have_iosbase])

  if test "$have_iosbase" = "yes"; then
    HAVE_IOSBASE="-DHAVE_IOSBASE"
    AC_DEFINE(HAVE_IOSBASE,1, [std::ios_base])
  else
    AC_DEFINE(HAVE_IOSBASE,0)
  fi
])

# Check if STL elements support the clear() method
AC_DEFUN([AC_CPP_STL_CLEAR],
[
  AC_MSG_CHECKING([for clear() method in STL objects])

  AC_CACHE_VAL(ac_cv_cpp_stl_clear,
  [
    AC_LANG_CPLUSPLUS
    cat > conftest.$ac_ext <<EOF
    #include <map>
    int main() {
      std::map<int, int, std::less<int> > m;
      m.clear();
      return 0;
    };
EOF

    if AC_TRY_EVAL(ac_link) && test -s conftest; then
      ac_cv_cpp_stl_clear="have_stl_clear=yes"
    else
      ac_cv_cpp_stl_clear="have_stl_clear=no"
    fi
    rm -f conftest*
  ])dnl
  eval "$ac_cv_cpp_stl_clear"

  AC_MSG_RESULT([$have_stl_clear])

  if test "$have_stl_clear" = "yes"; then
    HAVE_STL_CLEAR="-DHAVE_STL_CLEAR"
    AC_DEFINE(HAVE_STL_CLEAR,1, [STL Objects have a clear method])
  fi
])
