THIS IS BOBOT++ VERSION 2.3 (this file is also -*- text -*-)

=== REQUIREMENTS (Compile) === 

Guile Scheme (http://www.gnu.org/software/guile/) Version 1.8+ is used
for scripting. It is optional and may be disabled by passing
`--disable-scripting' to configure.

=== BACKGROUND ===

This is the new stable. Upgrading from 2.0 is recommended, especially
if you use the scripting interface.

=== BIG CHANGES FROM 2.0 ===

The biggest change is that all bot-* functions are now
bot:*. A simple 'perl -pi -e s/bot-/bot:/g SCRIPT1 SCRIPT2 .. SCRIPTN'
should be enough to update your scripts. The bot-* functions are
defined as aliases for the bot:* functions in bobot-utils.scm, which
is autoloaded by bobot++.

The binary is now named bobotpp because of automake choking on ++ in
rule names. In order for the bot to work properly, you must 'make
install' it so that bobot-utils.scm is installed and can be loaded for
scripts to use (unless you have Guile disabled).

=== IMPORTANT NOTES ===

2.2 should compile on GNU/Linux and FreeBSD. There are some
compatibility hacks for HP/UX, but they may not works as I don't have
access to an HP/UX machine. I'm not sure if it builds on other
systems, if it does please email me. If it doesn't, please email so I
can fix it.

=== DOCUMENTATION ===

There is a texinfo manual. It should be complete. "info bobot++" to
read it. It has sections on configuring, running, and scripting the
bot. Suggestions are welcome.

bobotpp --help is also helpful.

=== LICENSE ===

This program is distributed under the terms of the GNU General Public
License version 2 or later. See the COPYING file for details.

=== CONTACT INFO ===

Clinton Ebadi <clinton@unknownlamer.org> (Maintainer)
Official IRC Channel: #bobotpp on freenode
