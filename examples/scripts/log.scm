;;; simple logging script

(use-modules (ice-9 rdelim))

(define log-port (open-file "channels.log" "al"))

(define (time-stamp)
  (strftime "(%H:%M:%S) " (gmtime (current-time))))

(display (str-app
	  "\n== STARTING LOG [ " 
	  (strftime "%F " (gmtime (current-time)))
	  (time-stamp)
	  "] ==\n") log-port)

(let ((pub-func 
       (lambda (f t p)
	       (write-line (str-app
			    (string-downcase! t) " "
			    (time-stamp)
			    "<" f "> " p)
			   log-port))))
  (bot:addhook hooks/public ".*"
	     pub-func
	     10000 #t "log")
  (bot:addhook hooks/send/public ".*"
	       pub-func 1000 #t "log"))

(let ((act-hook
       (lambda (f t p)
	 (write-line
	  (str-app
	   (string-downcase! t) " "
	   (time-stamp)
	   "* " 
	   (car (string-split f #\!))
	   " " p)
	  log-port))))
  (bot:addhook hooks/action ".*"
	       act-hook
	       10000 #t "log")
  (bot:addhook hooks/send/action ".*"
	       act-hook 10000 #t "log"))

(bot:addhook hooks/join ".*.*"
 	     (lambda (name channel)
 	       (write-line (str-app 
			    (string-downcase! channel) " "
			    (time-stamp)
			    name " joined " 
			    (string-downcase! channel))
			   log-port))
	     10000 #t "log")

(bot:addhook hooks/kick ".*"
	     (lambda (who op channel reason)
	       (write-line
		(str-app
		 (string-downcase! channel) " "
		 (time-stamp)
		 who " was kicked from " channel
		 " by " op " (" reason ")") log-port))
	     10000 #t "log")

(bot:addhook hooks/mode ".*"
	     (lambda (who channel mode)
	       (write-line
		(str-app 
		 (string-downcase! channel) " "
		 (time-stamp)
		 who " set mode " mode
		 " on " (string-downcase! channel))
		log-port))
	     10000 #t "log")

(bot:addhook hooks/nickname ".*"
	     (lambda (orig new)
	       (write-line
		(str-app 
		 "--- "
		 (time-stamp)
		 orig " is now known as "
		 new) log-port))
	     10000 #t "log")

(bot:addhook hooks/leave ".*"
	     (lambda (who channel)
	       (write-line
		(str-app
		 (string-downcase! channel) " "
		 (time-stamp)
		 who " has left channel " 
			 (string-downcase! channel))
		log-port))
	     10000 #t "log")

(bot:addhook hooks/signoff ".*"
	     (lambda (who rest)
	       (write-line
		(str-app 
		 "--- "
		 (time-stamp)
		 who " has left IRC (" 
			 (substring rest 1 (string-length rest)) ")")
		log-port))
	     10000 #t "log")

(bot:addhook hooks/topic ".*"
	     (lambda (op channel new-topic)
	       (write-line
		(str-app 
		 (string-downcase! channel) " "
		 (time-stamp)
		 op " has changed the topic on "
			 (string-downcase! channel)
			 " to: " new-topic)
		log-port ))
	     10000 #t "log")

(add-hook! bot:exit-hook (lambda ()
			   (display  (str-app 
				      "== ENDING LOG [ " 
				      (strftime "%F " (gmtime (current-time)))
				      (time-stamp)
				      "] ==\n") log-port)))