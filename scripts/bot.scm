;;; Bobot++ Module for for giving access to bobot++ commands to other
;;; scheme modules

;;; This file is covered by the GPL version 2 or (at your option) any
;;; later version

(define-module (bobotpp bot))

(set-module-uses! (module-public-interface (current-module))
		  (list (module-ref (resolve-module '(guile-user) #f)
				    'the-bot-module)))
